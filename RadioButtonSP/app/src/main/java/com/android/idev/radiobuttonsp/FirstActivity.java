package com.android.idev.radiobuttonsp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

public class FirstActivity extends AppCompatActivity {

    private FrameLayout scrollFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        preUI();
    }
    private void preUI()
    {
        scrollFrameLayout = (FrameLayout) findViewById(R.id.frameScroll);
        loadScrollFragment();

    }

    private void loadScrollFragment() {
        Fragment scrollFragment = new ScrollFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameScroll, scrollFragment);
        fragmentTransaction.commit();
    }

}
