package com.android.idev.radiobuttonsp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by QMCPL on 25/01/17.
 */

public class ScrollFragment extends Fragment
{
    private View rootView = null;
    private LinearLayout secondHorizontalScrollView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.scroll_fragment, container, false);
            secondHorizontalScrollView = (LinearLayout) rootView.findViewById(R.id.secondHorizontalScrollView);
        }

        return rootView;
    }
}
