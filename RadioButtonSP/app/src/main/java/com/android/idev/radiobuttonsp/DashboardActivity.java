package com.android.idev.radiobuttonsp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity {

    Button buttonFirstActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        preUI();
        activityFirstPressed();
    }

    private void preUI() {
        buttonFirstActivity = (Button) findViewById(R.id.activity1Present);

    }

    private void activityFirstPressed() {
        buttonFirstActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                presentFirstActivity(); // To load first activity
            }
        });
    }

    private void presentFirstActivity() {
        Intent presentIntent = new Intent(DashboardActivity.this, FirstActivity.class);
        startActivity(presentIntent);
    }


}
