//: Playground - noun: a place where people can play

import UIKit

print("Hello, Firoz please run this playground","\n")


var dicGetter : [String : Any] = ["Firoz" : "Sudeep"]

var dicMain:Dictionary  <String, Any>
{
    get {return Dictionary()}

    set {
            print("Recived new value", newValue,"\n")
            dicGetter = newValue // Here is newValue is default syntax
        }
}

// Setting a value in main dictionary
dicMain = ["New value" : "Stored"] // Setting value
print(dicGetter,"\n")

dicMain = ["New value 2" : "Stored again"]
print(dicGetter,"\n")
print("\n")
