package idev.sharedpreferencedemo.Helpers;

/**
 * Created by QMCPL on 02/02/17.
 */
import android.content.Context;
import android.content.SharedPreferences;

import com.wizchen.topmessage.TopMessageManager;

public class SPDSingleton
{
    public static final String MY_PREFS_NAME = "mySharedPreference";

    private static SPDSingleton ourInstance = new SPDSingleton();

    public static SPDSingleton getInstance() {
        return ourInstance;
    }

    public static void setStringToSp(String value, String key, Context context)
    {

        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }


    public static String getStringFromSp(String key, Context context)
    {
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        //String restoredText = prefs.getString(key, "N/A");
        /*if (restoredText != null)
        {
            return restoredText;//"No name defined" is the default value.
        }else
        {
            return  "N/A";
        }*/

        return prefs.getString(key, "N/A");
    }

    public Boolean isEmailAddressValid(String emailTxt)
    {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(emailTxt).matches();

    }

    public  Boolean isBothStringSame(String strI, String strII)
    {
        if (strI.equals(strII))
        {
            return true;
        }else
        {
            return false;
        }

    }

    /* Toast message */
    public void showToastWithText(String txtString, Context contextPassed)
    {
        TopMessageManager.showWarning(txtString);
    }

    public void showSuccessToastWithText(String txtString, Context contextPassed)
    {
        TopMessageManager.showSuccess(txtString);
    }

    public void showToastWithTitleText(String txtString, String title)
    {
//        int duration = Toast.LENGTH_SHORT;
//        TopMessageManager.showSuccess(txtString, duration, title);
    }

}
