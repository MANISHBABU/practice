package idev.sharedpreferencedemo;

import android.app.Activity;
import android.app.Application;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.wizchen.topmessage.TopMessage;
import com.wizchen.topmessage.util.TopActivityManager;

/**
 * Created by QMCPL on 03/02/17.
 */

public class AppController extends Application implements Application.ActivityLifecycleCallbacks {

    private static final String LOG_TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
        registerActivityLifecycleCallbacks(TopActivityManager.getInstance());
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is created");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is started");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is resumed");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is paused");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is stopped");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " has saved instance state - " + bundle);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.e(LOG_TAG, activity.getLocalClassName() + " is destroyed");
    }
}
