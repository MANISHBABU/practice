package idev.sharedpreferencedemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import idev.sharedpreferencedemo.Helpers.SPDSingleton;

/**
 * Created by iDev on 02/02/17.
 */

public class RegistrationActivity extends Activity implements View.OnFocusChangeListener
{

    // Local variables declaration
    Button   registerBtn;
    EditText usernameTF;
    EditText emailIDTF;
    EditText passwordTF;
    EditText reenterPasswordTF;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        preUI();
        customListeners();
    }

    private void preUI()
    {
        // Assigning views
        registerBtn         = (Button)   findViewById(R.id.registerBTNRegistrationID);
        usernameTF          = (EditText) findViewById(R.id.usernameTFRegistrationID);
        emailIDTF           = (EditText) findViewById(R.id.emailTFRegistrationID);
        passwordTF          = (EditText) findViewById(R.id.passwordTFRegistrationID);
        reenterPasswordTF   = (EditText) findViewById(R.id.reenterPasswordTFRegistrationID);

        // Delegate for the listeners
        usernameTF.setOnFocusChangeListener(this);
        emailIDTF.setOnFocusChangeListener(this);
        passwordTF.setOnFocusChangeListener(this);
        reenterPasswordTF.setOnFocusChangeListener(this);

        usernameTF.setFocusableInTouchMode(true);
        emailIDTF.setFocusableInTouchMode(true);
        passwordTF.setFocusableInTouchMode(true);
        reenterPasswordTF.setFocusableInTouchMode(true);
    }

    private Boolean checkForValidationsListners()
    {
         if (SPDSingleton.getInstance().isBothStringSame(passwordTF.getText().toString(), reenterPasswordTF.getText().toString()) == false)
         {
             SPDSingleton.getInstance().showToastWithText("Password mismatched", getBaseContext());
             passwordTF.requestFocus();
             passwordTF.setText("");
             reenterPasswordTF.setText("");
             return  false;
         }else
         {
             if (usernameTF.length() <= 3)
             {
                 SPDSingleton.getInstance().showToastWithText("Please check the entered username", getBaseContext());
                 usernameTF.requestFocus();
                 return  false;
             }else if (SPDSingleton.getInstance().isEmailAddressValid(emailIDTF.getText().toString()) == false)
             {
                 SPDSingleton.getInstance().showToastWithText("Please check the entered email, it's validation failed", getBaseContext());
                 emailIDTF.requestFocus();
                 return  false;
             }else if (passwordTF.length() <= 3)
             {
                 SPDSingleton.getInstance().showToastWithText("Please check the entered password", getBaseContext());
                 passwordTF.requestFocus();
                 return  false;
             }
             else if (!SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString(), getApplicationContext()).equals("N/A"))
             {
                 // Check for username from Shared preference
                 String valueUsername = SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString(), getApplicationContext());
                 String valuePassword = SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString().concat("-p"), getApplicationContext());

                 SPDSingleton.getInstance().showToastWithText("Username is already taken", getApplicationContext());
                 usernameTF.requestFocus();
                 return  false;
             }


             return  true;
         }

    }

    private void saveAllValuesToSharedPreference()
    {
        // Username
        SPDSingleton.getInstance().setStringToSp(usernameTF.getText().toString(), usernameTF.getText().toString(), getApplicationContext());

        // Email address
        SPDSingleton.getInstance().setStringToSp(emailIDTF.getText().toString(), usernameTF.getText().toString().concat("-e"), getApplicationContext());

        // Password
        SPDSingleton.getInstance().setStringToSp(passwordTF.getText().toString(), usernameTF.getText().toString().concat("-p"), getApplicationContext());

    }


    private void customListeners()
    {
        registerBtn.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                // Saving all the parameters to user default after validation
                if (checkForValidationsListners() == true)
                {
                    // Saving all the values to shared preference
                    saveAllValuesToSharedPreference();
                    SPDSingleton.getInstance().showSuccessToastWithText("Succesfully registered", getApplicationContext());
                    finish(); // Closing the activity after the sucessfull login
                }
            }
        });

    }

    @SuppressWarnings("My warning - Ask for done button press listner")
    // For Toast and custom Toast see this link:-  http://stackoverflow.com/questions/3500197/how-to-display-toast-in-android


    @Override
    public void onFocusChange(View view, boolean b)
    {
        switch (view.getId())
        {
            case  R.id.usernameTFRegistrationID:
                if (view.hasFocus())
                {

                }
                break;

            case  R.id.emailTFRegistrationID:

                if (view.hasFocus())
                {
                    if (usernameTF.length() <= 3)
                    {
                        SPDSingleton.getInstance().showToastWithText("Please check the entered username", getApplicationContext());
                        usernameTF.requestFocus();
                    }else
                    {
                        if (!SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString(), getApplicationContext()).equals("N/A"))
                        {
                            // Check for username from Shared preference
                            String valueUsername = SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString(), getApplicationContext());
                            String valuePassword = SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString().concat("-p"), getApplicationContext());

                            SPDSingleton.getInstance().showToastWithText("Username already taken", getApplicationContext());
                            usernameTF.requestFocus();


                        }
                    }
                }
                break;

            case R.id.passwordTFRegistrationID:
                if (usernameTF.length() <= 3)
                {
                    SPDSingleton.getInstance().showToastWithText("Please check the entered username", getBaseContext());
                    usernameTF.requestFocus();
                }else if (SPDSingleton.getInstance().isEmailAddressValid(emailIDTF.getText().toString()) == false)
                {
                    SPDSingleton.getInstance().showToastWithText("Please check the entered email, it's validation failed", getBaseContext());
                    emailIDTF.requestFocus();
                }


                break;

            case R.id.reenterPasswordTFRegistrationID:
                if (usernameTF.length() <= 3)
                {
                    SPDSingleton.getInstance().showToastWithText("Please check the entered username", getBaseContext());
                    usernameTF.requestFocus();
                }else if (SPDSingleton.getInstance().isEmailAddressValid(emailIDTF.getText().toString()) == false)
                {
                    SPDSingleton.getInstance().showToastWithText("Please check the entered email, it's validation failed", getBaseContext());
                    emailIDTF.requestFocus();
                }else if (passwordTF.length() <= 3)
                {
                    SPDSingleton.getInstance().showToastWithText("Please check the entered password", getBaseContext());
                    passwordTF.requestFocus();
                }

                break;


        }
    }
}

