package idev.sharedpreferencedemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import idev.sharedpreferencedemo.Helpers.Constant;
import idev.sharedpreferencedemo.Helpers.SPDSingleton;


public class LoginActivity extends AppCompatActivity
{
    // Declare variables
    Button      loginButton;
    Button      registrationButton;
    EditText    usernameTF;
    EditText    passwordTF;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Calling pre user interface setup method.
        preUI();
    }

     /*Configuring UI and set up initial values*/
    void preUI()
    {
         loginButton            = (Button) findViewById(R.id.loginBtnID);
         registrationButton     = (Button) findViewById(R.id.registrationBtnID);
         usernameTF             = (EditText) findViewById(R.id.usernameTFLoginID);
         passwordTF             = (EditText) findViewById(R.id.passwordTFLoginID);
    
         customOnClickListner();
    }

    void customOnClickListner() {
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activityChangeIntent = new Intent(getApplicationContext(), RegistrationActivity.class);

                // currentContext.startActivity(activityChangeIntent);
               startActivity(activityChangeIntent);

            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if (!SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString(), getApplicationContext()).equals("N/A"))
                {
                    // Check for username from Shared preference
                    String valueUsername = SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString(), getApplicationContext());
                    String valuePassword = SPDSingleton.getInstance().getStringFromSp(usernameTF.getText().toString().concat("-p"), getApplicationContext());

                    if (valuePassword.equals(passwordTF.getText().toString()))
                    {
                        SPDSingleton.getInstance().showSuccessToastWithText("Login successful", getApplicationContext());
                    }else
                    {
                        SPDSingleton.getInstance().showToastWithText("Login failed", getApplicationContext());
                    }

                }else
                {
                    // Check for username from Shared preference
                    SPDSingleton.getInstance().showToastWithText("Login failed", getApplicationContext());
                }
            }
        });

    }

}
