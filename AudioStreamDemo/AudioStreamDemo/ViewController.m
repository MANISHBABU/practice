//
//  ViewController.m
//  AudioStreamDemo
//
//  Created by Manish on 1/29/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//


#import "ViewController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NSString *urlAudio = @"https://16653.mc.tritondigital.com/NPR_510298/media-session/a8d9b7f2-fbdf-45b7-95ea-e1787fac7889/anon.npr-mp3/npr/ted/2017/01/20170127_ted_podcast.mp3?orgId=1&d=3692&p=510298&story=511816479&t=podcast&e=511816479&ft=pod&f=510298";

@interface ViewController ()
{
    AVAudioPlayer *audioPlayer;
    AVPlayerItem * playerItem;
    AVPlayer *audioPlayerItem;
    BOOL playing;

}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self preSetup];
}

- (void)preSetup
{
    playing = NO;
//    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"in-the-storm" ofType:@"mp3"]];
    NSURL *url = [NSURL URLWithString:urlAudio];
//    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    playerItem = [[AVPlayerItem alloc] initWithURL:url];
    audioPlayerItem = [[AVPlayer alloc] initWithPlayerItem:playerItem];

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
}


- (IBAction)playButtonPressed:(id)sender
{
//    if ([audioPlayer isPlaying])
//    {
//        [audioPlayer stop];
//    }else
//    {
//        [audioPlayer play];
//    }
    if (playing == NO)
    {
        [audioPlayerItem pause];
        playing = NO;

    }else
    {
        [audioPlayerItem play];
        playing = YES;
    }

}

@end
