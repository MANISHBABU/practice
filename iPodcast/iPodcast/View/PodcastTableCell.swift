//
//  PodcastTableCell.swift
//  iPodcast
//
//  Created by MANISH_iOS on 27/01/17.
//  Copyright © 2017 Delaplex. All rights reserved.
//

protocol PodcastTableCellDelegate
{
    func playBtnPressed(idX : Int)
}

import UIKit

class PodcastTableCell: UITableViewCell
{

    @IBOutlet var podcastImageView: UIImageView!
    @IBOutlet var podcastTitle: UILabel!
    @IBOutlet var podcastMakeFavBtn: UIButton!
    @IBOutlet var podcastScheduleBtn: UIButton!
    @IBOutlet var podcastPlayBtn: UIButton!
    
    
    var customID : Int = 0
    var delegate : PodcastTableCellDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func podcastPlayButtonPressed(_ sender: Any)
    {
        if delegate != nil
        {
           delegate?.playBtnPressed(idX: customID)
        }
    }
    
}
