//
//  ViewController.swift
//  iPodcast
//
//  Created by MANISH_iOS on 27/01/17.
//  Copyright © 2017 Delaplex. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet var searchTF: UITextField!

   let storyBoardPodcast =  UIStoryboard(name: "Main", bundle: nil)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func searchKeywordButtonPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID") as! SearchResultViewController
        vc.isKeywordSearch = true
        vc.searchedKeyword = searchTF.text
        
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func entrepreneurshipBtnPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID")
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func businessStrategyBtnPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID")
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func financeBtnPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID")
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func kidsAndFamilyBtnPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID")
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func newsAndPoliticsBtnPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID")
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func tvAndFilmBtnPressed(_ sender: Any)
    {
        let vc = storyBoardPodcast.instantiateViewController(withIdentifier: "searchRVCID")
        self.present(vc, animated: true, completion: nil)

    }
    
    
}

