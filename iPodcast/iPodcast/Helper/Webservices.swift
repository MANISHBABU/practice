      //
      //  Webservices.swift
      // HeroEyez
      //
      //  Created by MANISH_iOS on 01/08/16.
      //  Copyright © 2016 Delaplex Softwares. All rights reserved.
      //
      
      import UIKit
      import Alamofire
      
      class Webservices: NSObject
      {
        static let sharedInstance = Webservices()
        let sharedApp  = UIApplication.shared
        
        override init()
        {
            super.init()
        }

        //MARK:- Normal hit
        func getResponseForURL(url : String, completionHandler :@escaping (_ response : AnyObject?, _ status : Bool, _ error : String?, _ time : Timeline?) -> Void)
        {
            
            Alamofire.request(url).responseJSON
                { response in

                    if let JSON = response.result.value
                    {
                        completionHandler(JSON as AnyObject?, true, nil, response.timeline)
                    }else
                    {
                        completionHandler(nil, false, (response.result.error?.localizedDescription)!, response.timeline)
                    }
                }

        }
        
      }
