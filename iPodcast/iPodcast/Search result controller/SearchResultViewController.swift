//
//  SearchResultViewController.swift
//  iPodcast
//
//  Created by MANISH_iOS on 27/01/17.
//  Copyright © 2017 Delaplex. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController
{

    @IBOutlet var pocastTableView: UITableView!
    var podcastArray =  [PODCAST]()
    var isKeywordSearch : Bool!
    var searchedKeyword : String!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        preUI()
        getUpdatedData()
    }

    func preUI()
    {
        pocastTableView.register(UINib(nibName: "PodcastTableCell", bundle: nil), forCellReuseIdentifier: "podcastTableCellID")
        self.automaticallyAdjustsScrollViewInsets = false
        pocastTableView .allowsMultipleSelection = false
        pocastTableView.tableFooterView = UIView()
        pocastTableView.estimatedRowHeight = 120.0
        pocastTableView.rowHeight = UITableViewAutomaticDimension
        pocastTableView.separatorColor = UIColor.gray

    }
    //https://www.npr.org/rss/podcast.php?id=510292
    func getUpdatedData()
    {
        var url = ""
        Webservices.sharedInstance.getResponseForURL(url: "")
        { (response, status, error, timeLine) in
         
            if status == true && response != nil
            {
                
                
            }else
            {
                // Error
            }
        }
    }
    
    func parseJSONData()
    {
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- Actions
    @IBAction func closeButtonPressed(_ sender: Any)
    {
        
    }
    

    //MARK:- TableView Delegates & Datasource
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return podcastArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : PodcastTableCell = tableView.dequeueReusableCell(withIdentifier: "podcastTableCellID", for: indexPath) as! PodcastTableCell
        
        let modelPodcast = podcastArray[indexPath.row]
        
        cell.customID = indexPath.row
        cell.podcastTitle.text = modelPodcast.title
        //cell.podcastImageView.
        cell .updateConstraintsIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
    }

}

class PODCAST: NSObject
{
    override init()
    {
        super.init()
    }
    
    var title = ""
    var feedLink = ""
    var imgURL = ""
    var customID = ""
    var category = CATEGORYPODCAST.general

}
enum CATEGORYPODCAST : Int
{
    case general = 0
    case entertainment
    case sports
    case politics
    
}
