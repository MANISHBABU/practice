//
//  ViewController.swift
//  MPCalloutView
//
//  Created by MANISH_iOS on 30/12/16.
//  Copyright © 2016 iDev. All rights reserved.
//



/******************--- Reference link ---******************
https://github.com/wircho/CustomMapViewCallout
******************--- Reference link ---******************/

import UIKit
import MapKit

private let reuseIdentifierPin = "customPinMP"

class ViewController: UIViewController, MKMapViewDelegate, CustomPinDelegate // Important
{
    @IBOutlet var baseMapView: MKMapView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        showAnnotationOnView()
    }

    // Custom functions 
    func showAnnotationOnView()
    {
        let annotation = MKPointAnnotation()
        annotation.title = "fKhan..."
        annotation.coordinate = CLLocationCoordinate2D(latitude: 45.500810, longitude: -73.569915)
        
        
        baseMapView.addAnnotation(annotation)
        
        baseMapView.showAnnotations([annotation], animated: true)
        
        baseMapView .selectAnnotation(annotation, animated: true) // Opening the custom callout view

    }
    
    func updatePinPosition(pin:CustomPin)
    {
        let defaultShift:CGFloat = 50
        let pinPosition = CGPoint(x: pin.frame.midX, y: pin.frame.midX)

        let y = pinPosition.y - defaultShift
        
        let controlPoint = CGPoint(x: pinPosition.x, y: y)
        
        let controlPointCoordinate = baseMapView.convert(controlPoint, toCoordinateFrom: baseMapView)
        
        baseMapView.setCenter(controlPointCoordinate, animated: true)
        
    }

    //MARK:- MKMapView Delegates
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        let pin = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifierPin) ?? CustomPin(annotation: annotation, reuseIdentifier: reuseIdentifierPin)
        pin.canShowCallout = false
        
        if let  pinCustom = pin as? CustomPin
        {
            pinCustom.delegate = self
        }
        
        pin.image = #imageLiteral(resourceName: "mapSearch")
        return pin
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
            if let mapPin = view as? CustomPin
            {
                updatePinPosition(pin: mapPin)
            }
    }
    
    // MARK:- Custom Pin Delegate
    func hitMeButtonPressed(_ sender: Any)
    {
        print("Button action on main view controller")
    }
}

