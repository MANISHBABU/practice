//
//  CustomPin.swift
//  CustomMapView
//
//  Created by Adolfo Rodriguez on 2015-08-20.
//  Copyright (c) 2015 Relevant. All rights reserved.
//

protocol CustomPinDelegate
{
    func hitMeButtonPressed(_ sender: Any)
}


import Foundation

import MapKit

class CustomPin: MKPinAnnotationView, CustomCalloutDelegate
{
    
    fileprivate var calloutView:CustomCallout?
    fileprivate var hitOutside:Bool = true
    
    var delegate : CustomPinDelegate!
    
    var preventDeselection:Bool
    {
        return !hitOutside
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        let calloutViewAdded = calloutView?.superview != nil
        self.image = #imageLiteral(resourceName: "mapSearch") //  Important setting image to annotation
        
        if (selected || !selected && hitOutside) {
            super.setSelected(selected, animated: animated)
        }
        
        self.superview?.bringSubview(toFront: self)
        
        
        //Firoz: The Stack Overflow answer simply writes calloutView = CalloutView
        // here. That means it creates an empty view, which is why you don't see anything.
        // Instead I am loading the view from the nib file Callout.xib,
        // And I am setting a label's text using the annotation's title.
        // Here you could also set images, buttons, etc, as long ad they are
        // in your nib file.
        
        if (calloutView == nil)
        {
            calloutView = Bundle.main.loadNibNamed("Callout", owner: nil, options: nil)?[0] as? CustomCallout
            
            calloutView?.titleLabel.text = self.annotation?.title!
        }
        
        calloutView?.delegate = self
        // Important
        
        if (self.isSelected && !calloutViewAdded) {
            addSubview(calloutView!)
            calloutView!.center = CGPoint(x: 10, y: -calloutView!.frame.size.height / 2.0)
        }
        
        if (!self.isSelected) {
            calloutView?.removeFromSuperview()
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView?
    {
        var hitView = super.hitTest(point, with: event)
        
        if let callout = calloutView
        {
            if (hitView == nil && self.isSelected) {
                hitView = callout.hitTest(point, with: event)
            }
        }
        
        hitOutside = hitView == nil
        
        return hitView;
    }
    
    // Important
    //MARK:- CalloutView delegate
    func hitMeButtonPressed(_ sender: Any)
    {
        if delegate != nil
        {
            delegate .hitMeButtonPressed(sender)
            // Important
        }
    }
}
