//
//  CustomCallout.swift
//  CustomMapView
//
//  Created by Adolfo Rodriguez on 2015-08-20.
//  Copyright (c) 2015 Relevant. All rights reserved.
//

protocol CustomCalloutDelegate
{
    func hitMeButtonPressed(_ sender: Any)
}

import Foundation

import MapKit

class CustomCallout: UIView
{
    
    
    @IBOutlet weak var titleLabel: UILabel!
    var delegate : CustomCalloutDelegate!
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView?
    {
        let point = point
        let viewPoint = superview?.convert(point, to: self) ?? point
        
        _ = self.point(inside: viewPoint, with: event)
        
        let view = super.hitTest(viewPoint, with: event)
        
        return view
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool
    {
        return bounds.contains(point)
    }
    
    // Actions
    @IBAction func hitMeButtonPressed(_ sender: Any)
    {
        print("Hit complete")
        if delegate != nil
        {
            delegate .hitMeButtonPressed(sender)
        }
    }
    
    
}
