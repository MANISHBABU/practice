//
//  GPAutocompleteViewController.swift
//  HeroEyez
//
//  Created by MANISH_iOS on 12/01/17.
//  Copyright © 2017 iDev Softwares. All rights reserved.
//



import UIKit

class GPAutocompleteViewController: UIViewController
{

    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)

    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(true)

    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Firoz call this function 
    //MARK:- Custom function
    func getUpdatedDataFromTempJSONURL()
    {
            //https://api.myjson.com/bins/1eu9cz
        
        Webservices.sharedInstance.getResponseForURL(url: "https://api.myjson.com/bins/1eu9cz")
        { (responseJSON, status, errorString, timeLine) in
            
            if status == false
            {
                // Show error
                
            }else
            {
                // Show result on table view after parsing the data
                print(responseJSON!)
                self.parseJSONFKModel(jsonData: responseJSON as! NSDictionary)
            }
        }

    }
    
    func parseJSONFKModel(jsonData : NSDictionary)
    {
        var myTotalDataArray = [FKJsonModel]()
        
        if let results = jsonData.value(forKey: "entry") as? NSArray
        {
            for result  in results
            {
                var birthDate = ""
                var gender = ""
                var modelID = ""
                var nameFamily = ""
                var nameGiven = ""
                var nameComplete : String = ""
                var tempField = ""

                
                if let res = result as? NSDictionary
                {
                    if let birthDateGet = res.value(forKey: "birthDate") as? String
                    {
                        birthDate = birthDateGet
                    }
                    
                    if let genderGet = res.value(forKey: "gender") as? String
                    {
                        gender = genderGet
                    }

                    if let modelIDGet = res.value(forKey: "id") as? String
                    {
                        modelID = modelIDGet
                    }
                    
                    if let name = res.value(forKey: "name") as? NSArray
                    {
                        if name.count > 0
                        {
                            if let nameObj = name[0] as? NSDictionary
                            {
                                // Assigning family
                                if let family = nameObj.value(forKey: "family") as? NSArray
                                {
                                    if family.count > 0
                                    {
                                        if let fName = family[0] as? String
                                        {
                                            nameFamily = fName
                                            nameComplete.append(fName)
                                        }
                                    }
                                }
                                
                                // Assigning family
                                if let given = nameObj.value(forKey: "given") as? NSArray
                                {
                                    if given.count > 0
                                    {
                                        if let gName = given[0] as? String
                                        {
                                            nameGiven = gName
                                            nameComplete.append(" ")
                                            nameComplete.append(gName)
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if let tempFieldGet = res.value(forKey: "active") as? String
                    {
                        tempField = tempFieldGet
                    }
                    
                    let fkObj = FKJsonModel(birthDate: birthDate, gender: gender, modelID: modelID, nameFamily: nameFamily, nameGiven: nameGiven, nameComplete: nameComplete, tempField: tempField)
                    
                    myTotalDataArray .append(fkObj)
                }
                
            }
        }

        for obj in myTotalDataArray
        {
            print(obj.nameComplete)
        }
        
        let filteredCategory = myTotalDataArray.filter { $0.nameFamily.lowercased() .contains("s") || $0.nameGiven.lowercased() .contains("s") }.flatMap { $0 }
        
        print("/n")
        
        for obj in filteredCategory
        {
            print(obj.nameComplete)
        }


    }
}

class FKJsonModel : NSObject, NSCoding
{
    var birthDate : String!
    var gender : String!
    var modelID : String!
    var nameFamily       : String!
    var nameGiven : String!
    var nameComplete : String!
    var tempField : String!

    
    init(birthDate: String, gender: String, modelID: String, nameFamily: String, nameGiven: String, nameComplete: String, tempField : String)
    {
        self.birthDate = birthDate
        self.gender = gender
        self.modelID       = modelID
        self.nameFamily = nameFamily
        self.nameGiven = nameGiven
        self.nameComplete = nameComplete
        self.tempField = tempField
    }
    
    required convenience init(coder aDecoder: NSCoder)
    {
        let birthDate           = aDecoder.decodeObject(forKey: "birthDate")
        let gender              = aDecoder.decodeObject(forKey: "gender")
        let modelID             = aDecoder.decodeObject(forKey: "modelID")
        let nameFamily          = aDecoder.decodeObject(forKey: "nameFamily")
        let nameGiven           = aDecoder.decodeObject(forKey: "nameGiven")
        let nameComplete                = aDecoder.decodeObject(forKey: "nameComplete")
        let tempField           = aDecoder.decodeObject(forKey: "tempField")
        
        self.init(birthDate: birthDate as! String, gender: gender as! String, modelID: modelID as! String, nameFamily: nameFamily as! String, nameGiven: nameGiven as! String , nameComplete: nameComplete as! String, tempField : tempField as! String)
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(birthDate, forKey: "birthDate")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(modelID, forKey: "modelID")
        aCoder.encode(nameFamily, forKey: "nameFamily")
        aCoder.encode(nameGiven, forKey: "nameGiven")
        aCoder.encode(nameComplete, forKey: "nameComplete")
        aCoder.encode(tempField, forKey: "tempField")
    }
}
